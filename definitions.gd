class_name Defs

enum WeaponType {
	NONE,
	STAFF,
	WAND,
	GAUNTLET,
}

enum ManaType {
	NONE,
	FIRE,
	WATER,
	EARTH,
}

static func weapon_type_str(wt):
	if wt == WeaponType.STAFF:
		return "STAFF"
	elif wt == WeaponType.WAND:
		return "WAND"
	elif wt == WeaponType.GAUNTLET:
		return "GAUNTLET"
	else:
		return "NONE"

static func mana_type_str(mt):
	if mt == ManaType.FIRE:
		return "FIRE"
	elif mt == ManaType.WATER:
		return "WATER"
	elif mt == ManaType.EARTH:
		return "EARTH"
	else:
		return "NONE"
