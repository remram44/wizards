extends KinematicBody

const WALK_SPEED = 8.0
const ACCEL = 0.2
const GRAVITY = 0.15
const ORIENT_FRICTION = 0.9  # 0: turn immediately, 1: don't turn at all
const INTERACT_LONG_PRESS_TIME = 0.6

export(Defs.WeaponType) var weapon_type = Defs.WeaponType.NONE setget _set_weapon_type
export(int, 1, 4) var player_id = 1 setget _set_player_id
export(int, 0, 10) var health = 10 setget _set_health
export(int, 0, 100) var mana = 0 setget _set_mana
export(Defs.ManaType) var mana_type = Defs.ManaType.FIRE

var on_floor = false
var speed = Vector3()
var cooldown = 0.0
var interact_pressed = null

onready var floor_ray = $FloorRay
onready var hud = $Hud

func _set_weapon_type(new_weapon_type):
	weapon_type = new_weapon_type
	if hud != null:
		$Staff.visible = false
		$Wand.visible = false
		$Gauntlet.visible = false
		if weapon_type == Defs.WeaponType.STAFF:
			$Staff.visible = true
		elif weapon_type == Defs.WeaponType.WAND:
			$Wand.visible = true
		elif weapon_type == Defs.WeaponType.GAUNTLET:
			$Gauntlet.visible = true

func _set_player_id(new_player_id):
	player_id = new_player_id
	if hud != null:
		hud.player_id = new_player_id

func _set_health(new_health):
	health = new_health
	if hud != null:
		hud.health = new_health

func _set_mana(new_mana):
	mana = new_mana
	if hud != null:
		hud.mana = new_mana

func _set_mana_type(new_mana_type):
	mana_type = new_mana_type
	if hud != null:
		hud.mana_type = new_mana_type

func _ready():
	hud.player_id = player_id
	hud.health = health
	_set_weapon_type(weapon_type)
	_set_player_id(player_id)
	_set_health(health)
	_set_mana(mana)
	_set_mana_type(mana_type)

func _physics_process(delta):
	if is_on_floor():
		on_floor = true
	elif not floor_ray.is_colliding():
		on_floor = false

	if on_floor:
		# Compute desired movement speed
		var move = Vector3()
		move.x += Input.get_action_strength(act("right"))
		move.x -= Input.get_action_strength(act("left"))
		move.z += Input.get_action_strength(act("down"))
		move.z -= Input.get_action_strength(act("up"))
		move *= WALK_SPEED

		# Orient towards movement
		if move.length_squared() > 0.05:
			var desired_bearing = atan2(-move.x, -move.z)
			rotation.y = desired_bearing + wrapf(rotation.y - desired_bearing, -PI, PI) * ORIENT_FRICTION

		# Update speed
		speed = speed + (move - speed) * ACCEL

		# Don't slide on slopes
		if speed.length_squared() < 0.1 * 0.1:
			speed.x = 0.0
			speed.z = 0.0

	if not is_on_floor():
		speed.y -= GRAVITY

	# Move
	speed = move_and_slide(speed, Vector3(0, 1, 0), true)

	# Action cooldown
	if cooldown > 0.0:
		cooldown -= delta

	# Detect short/long press of interact button
	if Input.is_action_just_pressed(act("interact")):
		interact_pressed = delta
	elif Input.is_action_pressed(act("interact")) and interact_pressed != null:
		interact_pressed += delta

		# Long press drops weapon
		if interact_pressed > INTERACT_LONG_PRESS_TIME:
			interact_pressed = null
			drop_current_weapon()

	# Pick up weapons
	var pickup_found = false
	for body in $PickupArea.get_overlapping_bodies():
		if body.get("pickup_type") != null:
			pickup_found = true
			# Short press picks up
			if (
				Input.is_action_just_released(act("interact")) and
				interact_pressed != null and
				interact_pressed < INTERACT_LONG_PRESS_TIME
			):
				drop_current_weapon()
				_set_weapon_type(body.pickup_type)
				print("player %d picked up %s" % [player_id, Defs.weapon_type_str(body.pickup_type)])
				body.queue_free()
	hud.pickup_hint = pickup_found

	# Pick up mana
	for body in $PickupArea.get_overlapping_areas():
		var pickup_mana = body.get("pickup_mana")
		if pickup_mana != null and pickup_mana != Defs.ManaType.NONE:
			print("player %d picked up %s mana" % [player_id, Defs.mana_type_str(pickup_mana)])
			if body.get("pickup_mana") == mana_type:
				_set_mana(mana + 50)
			else:
				_set_mana(50)
			_set_mana_type(body.get("pickup_mana"))
			body.picked_up()

	# Shoot
	if Input.is_action_just_pressed(act("shoot")):
		# TODO: Shoot
		print("player %d shooting %s mana with %s" % [player_id, Defs.mana_type_str(mana_type), Defs.weapon_type_str(weapon_type)])
		_set_mana(max(0, mana - 1))

func drop_current_weapon():
	var drop = null
	if weapon_type == Defs.WeaponType.STAFF:
		drop = load("res://StaffPickup.tscn").instance()
	elif weapon_type == Defs.WeaponType.WAND:
		drop = load("res://WandPickup.tscn").instance()
	elif weapon_type == Defs.WeaponType.GAUNTLET:
		drop = load("res://GauntletPickup.tscn").instance()
	_set_weapon_type(Defs.WeaponType.NONE)
	if drop != null:
		drop.transform = transform
		drop.translate_object_local(Vector3(0.0, 0.8, -2.0))
		drop.rotate_y(0.2 * PI)
		get_tree().root.get_node("Game").add_child(drop)

func act(name):
	return "p%d_%s" % [player_id, name]
