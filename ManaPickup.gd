extends Area

export(Defs.ManaType) var pickup_mana = Defs.ManaType.FIRE

func _ready():
	randomize()
	$Timer.start(rand_range(1.0, 10.0))

func _on_timeout():
	if get_overlapping_areas():
		var delay = rand_range(8.0, 18.0)
		print("colliding bodies, mana not spawning, waiting %fs" % delay)
		$Timer.start(delay)
		return

	var rnd = randi() % 3
	$Fire.visible = false
	$Water.visible = false
	$Earth.visible = false
	if rnd == 0:
		pickup_mana = Defs.ManaType.FIRE
		$Fire.visible = true
	elif rnd == 1:
		pickup_mana = Defs.ManaType.WATER
		$Water.visible = true
	else:
		pickup_mana = Defs.ManaType.EARTH
		$Earth.visible = true
	visible = true
	monitorable = true
	print("%s mana spawned" % Defs.mana_type_str(pickup_mana))

func picked_up():
	var delay = rand_range(3.0, 20.0)
	print("%s mana picked up, respawning in %fs" % [Defs.mana_type_str(pickup_mana), delay])
	pickup_mana = Defs.ManaType.NONE
	visible = false
	monitorable = false
	$Timer.start(delay)
