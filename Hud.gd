extends Control

const ANCHORS = [0.1, 0.35, 0.65, 0.9]

export(int, 1, 4) var player_id = 1 setget _set_player_id
export(int, 0, 10) var health = 10 setget _set_health
export(int, 0, 100) var mana = 0 setget _set_mana
export(Defs.ManaType) var mana_type = Defs.ManaType.FIRE setget _set_mana_type
export(bool) var pickup_hint = false setget _set_pickup_hint

func _set_player_id(new_player_id):
	player_id = new_player_id
	anchor_left = ANCHORS[player_id - 1]

func _set_health(new_health):
	health = new_health
	$Health.text = "Health: %d" % new_health

func _set_mana(new_mana):
	mana = new_mana
	update_mana_bar()

func _set_mana_type(new_mana_type):
	mana_type = new_mana_type
	update_mana_bar()

func _set_pickup_hint(new_pickup_hint):
	if pickup_hint != new_pickup_hint:
		pickup_hint = new_pickup_hint
		$PickupHint.visible = pickup_hint

func update_mana_bar():
	if mana <= 0:
		$Mana.text = ""
	else:
		$Mana.text = "Mana: %d %s" % [mana, Defs.mana_type_str(mana_type)]
